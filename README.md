# ARCHERON
Archéron, un jeu multijoueurs de type Roguelike, dans un univers médiéval fantastique ou SF, en vue à la 1ere personne ou à la 3e personne.

Le jeu est développé avec le moteur Unreal Engine.

## Quelques points clés :

### Histoire
Qui sait ?

### GamePlay immersif
- Vous jouez en temps réel, vous ne pouvez pas interrompre le déroulement du jeu.
- 1 vie et 1 seule par joueur.
- Univers évolutif pendant et en dehors des temps de jeu

### Génération procédurale et aléatoire
- Le contenu est généré aléatoirement : Les niveaux, Les armes, Les objets, les ennemis et les rencontres aléatoires. Exactement comme les fans de D&D ont l'habitude de jouer.

### Vue à la 1ere personne (FPS) ou à la 3e personne (TPS)
- Vous choisissez votre mode de vue préféré : FPS pour un jeu plus immersif, TPS pour profiter des animations de votre personnage et avoir une vue plus globale.

### Système de combat
- Vous combattez seuls ou en groupe, avec des alliés joueurs ou non-joueur (PNJ / Bots).
- Vous combattez en temps réel avec la possibilité de définir des tactiques et des stratégies au préalable.

### Niveau en extérieur et en intérieur
- En fonction de votre progression, vous serez amenés à jouer dans des donjons (aléatoires) ou des niveaux en extérieur.
- Le contenu ne sera pas modifié systématiquement, mais régulièrement afin de profiter de vos connaissances d'un lieu tout en permettant de le redécouvrir à nouveau de temps en temps.

### Compagnons
- Recruter des alliés parmi les PNJ ou les autres joueurs, ils peuvent se joindre à vos explorations.
### Copyrights

(C) 2022 GameaMea (http://www.gameamea.com)
